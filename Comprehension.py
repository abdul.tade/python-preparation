from typing import TypeVar, Iterator, Dict, List

K = TypeVar("K")
V = TypeVar("V")

def makeMap(keys: Iterator[K], values: Iterator[V]) -> Dict[K,V]:

    if __debug__:
        assert len(keys) == len(values), 'keys and values iterable must be of same length'
    
    ks = list(keys)
    vs = list(values)
    m = {}

    for i in range(len(ks)):
        m[ks[i]] = vs[i]

    return m

def main(args: List[str]):

    keys = [1,2,3,4]
    values =  ['a'*i for i in range(1, 5)]

    m = makeMap(keys,values)

    m2 = { chr(i) : i for i in range(0,100) if chr(i).isprintable() } # dictionary comprehension
    l = { i%15 for i in range(100) } # set comprehension

    mConcat = {**m, **m2}

    print(m)
    print(m2)
    # print(l)
    print(mConcat)

if __name__ == '__main__':
    print(vars())
    #main([])