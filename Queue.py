from typing import List, TypeVar, Iterable

T = TypeVar("T")

class Queue:

    def __init__(self, iterable: Iterable[T] = None) -> None:
        self._values = [] if iterable == None else list(iterable)
        self._len = len(self._values)

    def __len__(self) -> int:
        return self._len
    
    def __str__(self) -> str:
        return "Queue {\n\tvalues : %s\n\tlength : %d\n}"%(self._values, self._len)
    
    def is_empty(self):
        return self._len == 0
    
    def front(self) -> T:
        if self.is_empty():
            raise ValueError("queue is empty")
        return self._values[0]

    def enqueue(self,value: T) -> None:
        self._values.append(value)
        self._len += 1

    def dequeue(self) -> T:
        if self.is_empty():
            raise ValueError("Queue is empty")
        self._len -= 1
        return self._values.pop(0)
    

if __name__ == '__main__':

    q = Queue([1,2,3,4])
    q.enqueue(19)
    print(q)
    q.dequeue()
    print(q)