from abc import ABCMeta, abstractmethod


class Polygon(metaclass=ABCMeta):
    '''Abstract base class representing a polygon'''
    @abstractmethod
    def area(self) -> float:
        '''abstract area method'''
    
    @abstractmethod
    def perimeter(self) -> float:
        '''abstract perimeter method'''

    def num_sides(self) -> int:
        '''abstract number of sides method'''

class Triangle(Polygon):

    def __init__(self,base: float, height: float) -> None:
        assert (base >= 0) and (height >= 0), 'base and height must not be negative'
        self._base = base
        self._height = height

    @property
    def base(self) -> float:
        return self._base
    
    @property
    def base(self,val) -> None:
        if (val < 0):
            raise ValueError('value must me zero or positive')
        self._base = val

    @property
    def height(self) -> float:
        return self._height
    
    @property
    def height(self,val) -> None:
        if (val < 0):
            raise ValueError('value must me zero or positive')
        self._height = val

    def perimeter(self) -> float:
        return self._base*3
    
    def area(self) -> float:
        return 0.5 * self._base * self._height

 
def main():

    triangle = Triangle(10.0,20.234)
    print(triangle.area())
    print(triangle.perimeter())



if __name__ == '__main__':

    main()

