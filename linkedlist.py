from typing import TypeVar, Any

T = TypeVar("T")

class LinkedList:

    class Node:

        def __init__(self,value : T, next: Any) -> None:
            self._next = next
            self._data = value
        
        def __eq__(self, node: object) -> bool:
            if (node == None):
                return False
            return node._data == self._data
        
        def __ne__(self, node: object) -> bool:
            return not self.__eq__(node)
        
    def __init__(self):
        self._head = None
        self._length = 0

    def __repr__(self) -> str:
       return self.__str__()
    
    def __str__(self):
        results = []
        for num in self:
            results.append(str(num))
        return ' -> '.join(results)

    def __iter__(self):
        if self.is_empty:
            return
        head = self._head
        while head != None: 
            yield head._data
            head = head._next
    
    @property
    def is_empty(self) -> bool:
        return self._length == 0
    
    def top(self):
        if self.is_empty:
            raise ValueError("LinkedList is empty")
        return self._head
    
    def add(self, value: T) -> None:
        if self.is_empty:
            self._head = self.Node(value, None)
        else:
            self._head = self.Node(value, self._head)
        self._length += 1

    def pop(self) -> T:
        if self.is_empty:
            raise ValueError("LinkedList is empty")
        self._head = self._head._next
        self._length -= 1
    



if __name__ == '__main__':

    linkedlist = LinkedList()
    linkedlist.add(10)
    linkedlist.add(45)
    linkedlist.add(39)
    linkedlist.add(45)
    linkedlist.add(89)
    print(linkedlist)
    linkedlist.pop()
    linkedlist.pop()
    print(linkedlist)