import unittest
from typing import Iterable, Tuple

def is_multiple(n: int, m: int) -> bool:
    assert not ((n == 0) or (m == 0)), 'numbers must not be zero'
    n,m = abs(n),abs(m)
    return (n%m) == 0

def is_even(n: int) -> bool:
    nstr = str(n)
    return nstr[len(nstr)-1] in ['0','2']

def minmax(seq: Iterable[int]) -> Tuple[int]:
    nums = sorted(seq)
    return (nums[0],nums.pop())

def sumSquares(n: int) -> int:
    assert n >= 0, 'n must not be negative'
    return sum([i*i for i in range(n)])


class TestFunctions(unittest.TestCase):

    def test_equalValues(self):
        self.assertEqual(is_multiple(10,10), True)
    
    def test_unequalValues(self):
        self.assertFalse(is_multiple(15, 4))

    def test_negValues(self):
        self.assertTrue(is_multiple(-60, 5))

    def test_isEven(self):
        self.assertTrue(is_even(10))
        self.assertFalse(is_even(9))

    def test_minmax(self):
        arr = [3,5,2,5,2,54,6,15,5]
        t = minmax(arr)
        self.assertEqual(len(t),2)
        self.assertEqual(t[0],min(arr))
        self.assertEqual(t[1],max(arr))

    def test_sumSquares(self):
        self.assertRaises(AssertionError, sumSquares, -5)
        self.assertEqual(30,sumSquares(5))


if __name__ == '__main__':
    unittest.main()