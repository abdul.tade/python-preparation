from typing import TypeVar, List, Iterable, Any
import unittest

T = TypeVar("T")

class Stack:

    '''Datastructure simulates a stack'''

    def __init__(self, iterable : Iterable[Any] = None) -> None:
        self._values : List[Any] = [] if iterable == None else list(iterable)
        self._len = len(self._values)

    def __len__(self):
        return self._len
    
    def is_empty(self):
        return self._len == 0

    def top(self) -> T:
        if self.is_empty():
            raise ValueError("Stack is empty")
        return self._values[self._len-1]
    
    def push(self, value: T) -> None:
        self._values.append(value)
        self._len += 1
    
    def pop(self) -> T:
        if self.is_empty():
            raise ValueError("Stack is empty")
        self._len -= 1
        return self._values.pop()

class StackTest(unittest.TestCase):

    def test_len(self):
        self.assertEqual(len(Stack([1,2,3,4])), 4)
        self.assertEqual(len(Stack()), 0)

    def test_is_empty(self):
        stack = Stack([1,2,3,4])
        self.assertFalse(stack.is_empty())
        self.assertTrue(Stack().is_empty())

    def test_top(self):
        stack = Stack((1,2,3,4))
        self.assertEqual(stack.top(), 4)
        stack.pop()
        self.assertEqual(stack.top(), 3)
        self.assertRaises(ValueError, Stack().top)
    
if __name__ == '__main__':

    unittest.main()

        