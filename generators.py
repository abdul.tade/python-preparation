from typing import TypeVar, List, Iterable

T = TypeVar("T")


def shift(l: Iterable[T], new: T) -> Iterable[T]:
    assert (len(l) > 0), "List cannot be empty"
    l.remove(l[0])
    l.append(new)
    return l


def bulkshift(l: Iterable[T], new: Iterable[T]) -> Iterable[T]:
    return [shift(l, n) for n in new].pop()


def series(f: callable, histLen: int):
    '''f: a callable that takes an index and a list of arguments

       histLen: how long the history should be

       >>> def f(i: int, l: list[int]):
                return (2*i) + sum(l, 0)
            g = series(f,3) # g is a generator for the series
            result = next(g) # call till StopIteration Exception
       <<<
    '''
    index: int = 1
    history = [0 for i in range(histLen)]
    while True:
        print(history)
        result = f(index, history)
        history = history if histLen == 0 else shift(history, result)
        yield (index, result)
        index += 1


def main(args: List[str]):

    def func(i, l): return 2*i + sum(l, 0)

    g = series(func, 3)
    print(next(g))
    print(next(g))
    print(next(g))
    print(next(g))

    l = [x for x in range(10)]
    print(f'Before : {l}')
    print('After: ', bulkshift(l, [9.0, 8.4, 'SDD', 9, 'a', ]))


if __name__ == '__main__':
    main([])
