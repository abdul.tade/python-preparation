from typing import Iterator, TypeVar, List, Generic

T = TypeVar("T")


class Array(Generic[T]):
    
    def __init__(self,iterable: Iterator[T]) -> None:
        allSameTypes = all([ type(item) == type(T) for item in iterable ])
        if not allSameTypes:
            raise TypeError("All values should be of the same type")
        self._values = list(iterable)
        self._size = 0
        self._capacity = len(self._values)
        self._frozen = False # a frozen array has a fixed size

    def __init__(self, size: int):
        self._capacity = size
        self._values : List[int] = []
        self._typestr = type(T)
    
    def push(self, value: T):
        assert type(value) == type(T), "type must be same"
        assert not self._frozen, "Frozen array is not resizable"
        self._values.append(value)
        self._size += 1

    def pop(self) -> T:
        assert len(self._values) == 0, "array is empty"
        return self._values.pop()

    
